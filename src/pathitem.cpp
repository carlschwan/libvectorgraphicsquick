/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "pathitem.h"
#include <tessellation.rs.h>
#include <limits>
#include "noderesizehandle.h"

#include <QQmlEngine>
#include <QSGGeometry>
#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>

namespace Vector
{

PathItem::PathItem(QQuickItem *parent)
    : Item(parent)
{
    setWidth(110);
    setHeight(60);
    setFlag(ItemHasContents, true);

    m_commands << MoveTo { LyonPoint{0.0, 0.0} }
               << LineTo { LyonPoint{0.0, 40.0} }
               << LineTo { LyonPoint{40.0, 40.0} }
               << CubicBezierTo{ LyonPoint{70.0, 40.0}, LyonPoint{70.0, 0.0}, LyonPoint{ 50.0, 20.0} }
               << LineTo { LyonPoint{40.0, 0.0} }
               << Close {};

    connect(this, &Item::selectedChanged, this, [this]() {
        if (selected()) {
            showHandles();
        } else {
            hideHandles();
        }
    });
    setAntialiasing(true);
    nodePositionChanged();
}

PathItem::~PathItem()
{
}

void PathItem::componentComplete()
{
    QQuickItem::componentComplete();
    QQmlEngine *engine = qmlEngine(this);
    m_componentHandle = new QQmlComponent(engine, QUrl("qrc:/Handle.qml"));
}

void PathItem::showHandles()
{
    if (m_commands.empty()) {
        return;
    }

    // Close as first element should be filtered out when parsing.
    assert(!std::holds_alternative<Close>(m_commands[0]));

    for (auto &section : m_commands) {
        addHandles(section);
    }
}

void PathItem::hideHandles()
{
    // TODO recycle instead of deleting
    qDeleteAll(m_handles);
}

QList<QPointF> PathItem::nodes() const
{
    return m_nodes;
}

QSGNode *PathItem::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
    QSGGeometryNode *node = nullptr;
    QSGGeometry *geometry = nullptr;

    if (!oldNode) {
        node = new QSGGeometryNode;
        geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(),
                                   m_geometry.vertices.size(), m_geometry.indices.size());
        geometry->setIndexDataPattern(QSGGeometry::StaticPattern);
        geometry->setDrawingMode(GL_TRIANGLES);
        node->setGeometry(geometry);
        node->setFlag(QSGNode::OwnsGeometry);

        QSGFlatColorMaterial *material = new QSGFlatColorMaterial;
        material->setColor(QColor(255, 0, 0));
        node->setMaterial(material);
        node->setFlag(QSGNode::OwnsMaterial);
    } else {
        node = static_cast<QSGGeometryNode *>(oldNode);
        geometry = node->geometry();
        geometry->allocate(m_geometry.vertices.size(), m_geometry.indices.size());
    }

    QSGGeometry::Point2D *points = geometry->vertexDataAsPoint2D();
    std::size_t i = 0;
    for (const auto &vertice: m_geometry.vertices) {
        points[i].set(vertice.x, vertice.y);
        i++;
    }

    quint16* indices = geometry->indexDataAsUShort();
    i = 0;
    for (const auto indice: m_geometry.indices) {
        indices[i] = indice;
        i++;
    }

    node->markDirty(QSGNode::DirtyGeometry);
    return node;
}

void PathItem::nodePositionChanged()
{
    auto lyonBuilder = new_builder();
    for (const auto &command: qAsConst(m_commands)) {
        std::visit(overloaded {
            [&lyonBuilder](MoveTo moveTo) { lyonBuilder->move_to(moveTo.to); },
            [&lyonBuilder](LineTo lineTo) { lyonBuilder->line_to(lineTo.to); },
            [&lyonBuilder](CubicBezierTo cubicBezierTo) { lyonBuilder->cubic_bezier_to(cubicBezierTo.ctrl1, cubicBezierTo.ctrl2, cubicBezierTo.to); },
            [&lyonBuilder](Close) { lyonBuilder->close(); },
        }, command);
    }
    m_geometry = build_fill(std::move(lyonBuilder));

    // use vertices position to know min and max
    const auto [min, max] = minmax_point(m_geometry.vertices);

    qDebug() << min.x << min.y;

    qDebug() << "old x" << x() << "new x" << (x() - min.x);

    // update position
    setX(x() + min.x);
    setY(y() + min.y);
    setWidth(max.x - min.x);
    setHeight(max.y - min.y);

    for (auto &vertice: m_geometry.vertices) {
        vertice.x -= min.x;
        vertice.y -= min.y;
    }

    for (const auto &command: qAsConst(m_commands)) {
        std::visit(overloaded {
            [&min](MoveTo moveTo) { moveTo.to - min; },
            [&min](LineTo lineTo) { lineTo.to - min; },
            [&min](CubicBezierTo cubicBezierTo) { 
                cubicBezierTo.ctrl1 - min;
                cubicBezierTo.ctrl2 - min;
                cubicBezierTo.to - min;
            },
            [](Close) { },
        }, command);
    }

    // Update visual representation
    update();
}

void PathItem::addHandles(PathSection &section)
{
    if (std::holds_alternative<Close>(section)) {
        return;
    }
    // extract node position from command
    std::visit(overloaded {
        [](Close) {
            Q_ASSERT(false && "Should not happen");
        },
        [this, &section](auto &arg) {
            NodeResizeHandle *handle = new NodeResizeHandle();
            handle->setParentItem(this);

            const static qreal SIZE = 10;
            handle->setX(arg.to.x - SIZE / 2);
            handle->setY(arg.to.y - SIZE / 2);
            handle->setScale(1);
            handle->setZ(20);

            connect(handle, &QQuickItem::xChanged, this, [&arg, handle, this]() {
                arg.to.x = handle->x() + 5;
                nodePositionChanged();
            });
            connect(handle, &QQuickItem::yChanged, this, [&arg, handle, this]() {
                arg.to.y = handle->y() + 5;
                nodePositionChanged();
            });

            m_handles.append(handle);
        }
    }, section);
}

void PathItem::addPathSection(int index, PathSection pathSection)
{
    m_commands.insert(index, std::move(pathSection));
    if (selected()) {
        addHandles(m_commands[index]);
    }
}

void PathItem::mouseDoubleClickEvent(QMouseEvent *event)
{
    addPathSection(3, LineTo { LyonPoint { 100, 100 }});
    update();
}
#include "moc_pathitem.cpp"
}
