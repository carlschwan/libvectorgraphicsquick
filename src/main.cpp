#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>

#include "resizehandle.h"
//#include "noderesizehandle.h"
#include "item.h"
#include "pathitem.h"
#include "scene.h"
#include "pane.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("LibVectorEditorQuick");
    const char *uri = "org.kde.vectoreditor";
    qmlRegisterType<Vector::ResizeHandle>(uri, 1, 0, "ResizeHandle");
    //qmlRegisterType<Vector::NodeResizeHandle>(uri, 1, 0, "NodeResizeHandle");
    qmlRegisterType<Vector::Item>(uri, 1, 0, "Item");
    qmlRegisterType<Vector::PathItem>(uri, 1, 0, "PathItem");
    qmlRegisterType<Vector::Scene>(uri, 1, 0, "Scene");
    qmlRegisterType<Vector::Pane>(uri, 1, 0, "Pane");

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
