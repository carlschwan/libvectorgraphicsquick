/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include <QQuickItem>

namespace Vector
{
class Pane;

class Item: public QQuickItem
{
    Q_OBJECT

    /**
     * Qml component used in the sidebar when this Item is selected
     */
    Q_PROPERTY(QQmlComponent *editor READ editor WRITE setEditor NOTIFY editorChanged)

    Q_PROPERTY(bool selected READ selected NOTIFY selectedChanged)

public:
    Item(QQuickItem *parent = nullptr);
    ~Item() = default;

    QQmlComponent *editor() const;
    void setEditor(QQmlComponent *editor);

    bool selected() const;
    // not accessible from QML
    void setSelected(bool selected);


protected:
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent * event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

Q_SIGNALS:
    void editorChanged();
    void selectedChanged();

private:
    QQmlComponent *m_editor;
    Pane *m_pane;
    bool m_selected = false;

    QPointF m_mouseDownPosition;
    QPointF m_mouseDownGeometry;
};

}

Q_DECLARE_METATYPE(Vector::Item *);
