/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "scene.h"
#include "item.h"
#include "pane.h"

namespace Vector
{

Scene::Scene(QQuickItem *parent)
    : QQuickItem(parent)
{
    setFocus(true);
    qreal itemZ = 3;
    m_horizonalRuler = new Ruler(Qt::Horizontal, this);
    m_horizonalRuler->setObjectName("HorizontalRuler");
    m_horizonalRuler->setZ(itemZ++);
    m_horizonalRuler->setVisible(true);
    m_horizonalRuler->setX(0);
    m_horizonalRuler->setY(0);
    
    m_verticalRuler = new Ruler(Qt::Vertical, this);
    m_verticalRuler->setObjectName("VerticalRuler");
    m_verticalRuler->setDrawCorner(true);
    m_verticalRuler->setZ(itemZ++);
    m_verticalRuler->setVisible(true);
    m_verticalRuler->setX(0);
    m_verticalRuler->setY(0);
}

void Scene::componentComplete()
{
    QQuickItem::componentComplete();
    m_horizonalRuler->setSize(QSizeF(width(), 20));
    m_verticalRuler->setSize(QSizeF(20, height()));
    
    connect(this, &Scene::heightChanged, this, [this]() {
        m_horizonalRuler->setSize(QSizeF(width(), 20));
        m_verticalRuler->setSize(QSizeF(20, height()));
    });
    connect(this, &Scene::widthChanged, this, [this]() {
        m_horizonalRuler->setSize(QSizeF(width(), 20));
        m_verticalRuler->setSize(QSizeF(20, height()));
    });
}

void Scene::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Control) {
        m_controlPressed = true;
    }
    event->accept();
}

void Scene::keyReleaseEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Control) {
        m_controlPressed = false;
    }
    event->accept();
}

void Scene::wheelEvent(QWheelEvent* event)
{
    const QPoint pixelDelta = event->pixelDelta();
    if (!pixelDelta.isNull()) {
        m_pane->setScale(m_pane->scale() + pixelDelta.y() * 0.01);
    }
    event->accept();
}

Pane * Scene::pane() const
{
    return m_pane;
}

void Scene::setPane(QQuickItem* pane)
{
    auto p = qobject_cast<Pane *>(pane);
    if (p == m_pane) {
        return;
    }
    
    m_pane = p;
    
    Q_ASSERT(m_pane != nullptr);
    m_pane->setX(30);
    m_pane->setY(30);
    m_pane->setSize(QSizeF(1920, 1080));
    m_pane->setVisible(true);
    m_pane->setParentItem(this);
    Q_EMIT paneChanged();
}


#include "moc_scene.cpp"

}
