// SPDX-FileCopyrightText: 2019 Marco Martin <mart@kde.org>
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include <QQuickItem>
#include <tessellation.rs.h>

namespace Vector
{

class PathItem;

class NodeResizeHandle: public QQuickItem
{
    Q_OBJECT
public:
    NodeResizeHandle(QQuickItem *parent = nullptr);
    ~NodeResizeHandle() = default;

    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    QPointF m_mouseDownPosition;
    QRectF m_mouseDownGeometry;

    QPointer<QQmlComponent> m_component;
};

}
