/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "item.h"
#include "pane.h"

#include <cmath>

namespace Vector
{

Item::Item(QQuickItem *parent)
    : QQuickItem(parent)
{
    setAcceptedMouseButtons(Qt::LeftButton);
    
    QQuickItem *candidate = parent;
    while (candidate) {
        auto pane = qobject_cast<Pane *>(candidate);
        if (pane) {
            m_pane = pane;
            break;
        }

        candidate = candidate->parentItem();
    }
    
    connect(this, &QQuickItem::parentChanged, this, [this]() {
        auto candidate = parentItem();
        while (candidate) {
            auto pane = qobject_cast<Pane *>(candidate);
            if (pane) {
                m_pane = pane;
                break;
            }

            candidate = candidate->parentItem();
        }
        Q_ASSERT(m_pane != nullptr && "A item should be located in a pane");
    });
}

QQmlComponent *Item::editor() const
{
    return m_editor;
}

void Item::setEditor(QQmlComponent* editor)
{
    if (editor == m_editor) {
        return;
    }
    
    m_editor = editor;
    Q_EMIT editorChanged();
}

bool Item::selected() const
{
    return m_selected;
}

void Item::setSelected(bool selected)
{
    if (selected == m_selected) {
        return;
    }
    
    m_selected = selected;
    Q_EMIT selectedChanged();
}

void Item::mouseReleaseEvent(QMouseEvent *event)
{
    m_pane->setSelectedItem(this);
    event->accept();
}

void Item::mousePressEvent(QMouseEvent *event)
{
    m_mouseDownPosition = event->windowPos();
    m_mouseDownGeometry = QPointF(x(), y());
    event->accept();
}

void Item::mouseMoveEvent(QMouseEvent *event)
{
    const QPointF difference = m_mouseDownPosition - event->windowPos();
    const qreal x = m_mouseDownGeometry.x() - difference.x();
    const qreal y = m_mouseDownGeometry.y() - difference.y();
    setX(x);
    setY(y);
}

#include "moc_item.cpp"

}
