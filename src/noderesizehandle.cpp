/*
 *   Copyright 2019 by Marco Martin <mart@kde.org>
 *   Copyright 2020 by Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "noderesizehandle.h"
#include "pathitem.h"

#include <QCursor>
#include <cmath>
#include <QSGGeometry>
#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>

namespace Vector
{

NodeResizeHandle::NodeResizeHandle(QQuickItem *parent)
    : QQuickItem(parent)
{
    setAcceptedMouseButtons(Qt::LeftButton);
    setCursor(Qt::SizeBDiagCursor);
    setFlag(ItemHasContents, true);
    setWidth(10.0);
    setHeight(10.0);
}

void NodeResizeHandle::mousePressEvent(QMouseEvent *event)
{
    m_mouseDownPosition = event->windowPos();
    m_mouseDownGeometry = QRectF(x(), y(), width(), height());
    event->accept();
}

void NodeResizeHandle::mouseMoveEvent(QMouseEvent *event)
{
    const QPointF difference = m_mouseDownPosition - event->windowPos();

    m_mouseDownPosition = event->windowPos();

    const int lx = x() - difference.x() + 5;
    const int ly = y() - difference.y() + 5;

    setX(x() - difference.x());
    setY(y() - difference.y());

    event->accept();
}

void NodeResizeHandle::mouseReleaseEvent(QMouseEvent *event)
{
    event->accept();
}

QSGNode *NodeResizeHandle::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
    QSGGeometryNode *node = nullptr;
    QSGGeometry *geometry = nullptr;

    auto lyonBuilder = new_builder();
    lyonBuilder->move_to(LyonPoint{0.0, 0.0});
    lyonBuilder->line_to(LyonPoint{10.0, 0.0});
    lyonBuilder->line_to(LyonPoint{10.0, 10.0});
    lyonBuilder->line_to(LyonPoint{0.0, 10.0});
    lyonBuilder->line_to(LyonPoint{0.0, 0.0});
    const auto &geo = build_fill(std::move(lyonBuilder));

    if (!oldNode) {
        node = new QSGGeometryNode;
        geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(),
                                   geo.vertices.size(), geo.indices.size());
        geometry->setIndexDataPattern(QSGGeometry::StaticPattern);
        geometry->setDrawingMode(GL_TRIANGLES);
        node->setGeometry(geometry);
        node->setFlag(QSGNode::OwnsGeometry);

        QSGFlatColorMaterial *material = new QSGFlatColorMaterial;
        material->setColor(QColor(200, 200, 200));
        node->setMaterial(material);
        node->setFlag(QSGNode::OwnsMaterial);
    } else {
        node = static_cast<QSGGeometryNode *>(oldNode);
        geometry = node->geometry();
        geometry->allocate(geo.vertices.size(), geo.indices.size());
    }

    QSGGeometry::Point2D *points = geometry->vertexDataAsPoint2D();
    std::size_t i = 0;
    for (const auto &vertice: geo.vertices) {
        points[i].set(vertice.x, vertice.y);
        i++;
    }

    quint16* indices = geometry->indexDataAsUShort();
    i = 0;
    for (const auto indice: geo.indices) {
        indices[i] = indice;
        i++;
    }

    node->markDirty(QSGNode::DirtyGeometry);
    return node;
}

#include "moc_noderesizehandle.cpp"

}
