/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "propertymodel.h"

namespace Vector
{
    
PropertyModel::PropertyModel(QObject* parent)
    : QAbstractTableModel(parent)
{
}


int PropertyModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return m_frameCount;
}

int PropertyModel::frameCount() const
{
    return m_frameCount;
}

void PropertyModel::setFrameCount(int frameCount)
{
    if (m_frameCount == frameCount) {
        return;
    }
    
    m_frameCount = frameCount;
    
    // TODO update values
    Q_EMIT frameCountChanged();
}

int PropertyModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return m_properties.count();
}

QVariant PropertyModel::data(const QModelIndex& index, int role) const
{
    if (role != Qt.DisplayRole) {
        return QVariant();
    }
    
    if (!m_values.contains(index.row()) || !m_values[index.row()].contains(index.column())) {
        return QVariant();
    }
    
    return m_values[index.row()][index.column()];
}

QVariant PropertyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt.DisplayRole or orientation != Qt.Horizontal) {
        return QVariant();
    }
    
    return m_properties[section];
}

#include "moc_propertymodel.cpp"

}
