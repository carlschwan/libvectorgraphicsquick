#pragma once

#include <QSGGeometryNode> 

class QSGFlatColorMaterial;
class QSGGeometry;
class QRectF;

class SelectedItemNode : public QSGGeometryNode {
public:
    SelectedItemNode();
    ~SelectedItemNode() = default;
    
    void setRect(const QRectF &rect);
    
private:
    QSGGeometry *m_geometry = nullptr;
    QSGFlatColorMaterial *m_material = nullptr;
    QRectF m_rect;
};
