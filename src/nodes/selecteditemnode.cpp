#include "selecteditemnode.h"

#include <QColor>
#include <QSGFlatColorMaterial>
#include <QSGFlatColorMaterial> 
#include <QDebug>

SelectedItemNode::SelectedItemNode()
{
    m_geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
    m_geometry->setDrawingMode(GL_LINE_LOOP);
    m_geometry->setLineWidth(1);
    setGeometry(m_geometry);
    
    m_material = new QSGFlatColorMaterial;
    m_material->setColor(QColor(0, 0, 0));
    setMaterial(m_material);
    
    setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
    
    auto leftHandle = new QSGGeometryNode();
    auto geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4, 2 * 3);
    geometry->setDrawingMode(GL_TRIANGLES);
    geometry->setIndexDataPattern(QSGGeometry::StaticPattern);
    leftHandle->setGeometry(geometry);
    geometry->vertexDataAsPoint2D()[0].set(-4, -4);
    geometry->vertexDataAsPoint2D()[1].set(-4, 4);
    geometry->vertexDataAsPoint2D()[2].set(4, 4);
    geometry->vertexDataAsPoint2D()[4].set(4, -4);
    
    geometry->indexDataAsUShort()[0] = 0;
    geometry->indexDataAsUShort()[1] = 1;
    geometry->indexDataAsUShort()[2] = 2;
    geometry->indexDataAsUShort()[3] = 0;
    geometry->indexDataAsUShort()[4] = 1;
    geometry->indexDataAsUShort()[5] = 3;
    markDirty(QSGNode::DirtyGeometry);
    
    auto material = new QSGFlatColorMaterial;
    material->setColor(QColor(0, 0, 0));
    leftHandle->setMaterial(m_material);
    
    appendChildNode(leftHandle);
}

void SelectedItemNode::setRect(const QRectF& rect)
{
    if (m_rect != rect) {
        m_rect = rect;
        m_geometry->vertexDataAsPoint2D()[0].set(0, 0);
        m_geometry->vertexDataAsPoint2D()[1].set(0, m_rect.height());
        m_geometry->vertexDataAsPoint2D()[2].set(m_rect.width(), m_rect.height());
        m_geometry->vertexDataAsPoint2D()[3].set(m_rect.width(), 0);
        
        qDebug() << m_rect.height() << this;
        m_geometry->markVertexDataDirty();
        m_geometry->markIndexDataDirty();
        markDirty(QSGNode::DirtyGeometry);
    }
}


