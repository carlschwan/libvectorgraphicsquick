#include "rectanglenode.h"

#include <QColor>
#include <QSGFlatColorMaterial>
#include <QSGFlatColorMaterial> 
#include <QDebug>

RectangleNode::RectangleNode()
{
    setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
    
    m_geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4, 2 * 3);
    m_geometry->setDrawingMode(GL_TRIANGLES);
    m_geometry->setIndexDataPattern(QSGGeometry::StaticPattern);
    setGeometry(m_geometry);
    
    m_material = new QSGFlatColorMaterial;
    m_material->setColor(QColor(0, 0, 0));
    setMaterial(m_material);
}

void RectangleNode::setRect(const QRectF& rect)
{
    if (m_rect != rect) {
        m_rect = rect;
        m_geometry->vertexDataAsPoint2D()[0].set(0, 0);
        m_geometry->vertexDataAsPoint2D()[1].set(0, m_rect.height());
        m_geometry->vertexDataAsPoint2D()[2].set(m_rect.width(), m_rect.height());
        m_geometry->vertexDataAsPoint2D()[4].set(m_rect.width(), 0);
        
        m_geometry->indexDataAsUShort()[0] = 0;
        m_geometry->indexDataAsUShort()[1] = 1;
        m_geometry->indexDataAsUShort()[2] = 2;
        m_geometry->indexDataAsUShort()[3] = 0;
        m_geometry->indexDataAsUShort()[4] = 1;
        m_geometry->indexDataAsUShort()[5] = 3;
        markDirty(QSGNode::DirtyGeometry);
    }
}


