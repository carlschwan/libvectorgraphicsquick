/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#pragma once

#include <QAbstractTableModel>

namespace Vector
{

class PropertyModel : public QAbstractTableModel
{
    Q_OBJECT
    
    Q_PROPERTY(int frameCount READ frameCount WRITE setFrameCount NOTIFY frameCountChanged FINAL)
    
public:
    PropertyModel(QObject *parent = 0);
    
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    
    int frameCount() const;
    void setFrameCount(int frameCount);
    
Q_SIGNALS:
    void frameCountChanged();
    
private:
    QVector<QString> m_properties;
    // map from propery name to a map from frame to property value
    QMap<int, QMap<int, qreal>> m_values;
    int m_frameCount;
};
}
