// SPDX-FileCopyrightText: 2020 Carl Schwan <carlschwan@kde.org>
//
// SPDX-License-Identifier: LGPL-2.1-or-later
#include <vector>
#include <variant>
#include <tessellation.rs.h>
#include <QVector>
#include <optional>
#include <QPainterPath>

/// Move to the point without drawing a line.
struct MoveTo
{
    /// The destination.
    LyonPoint to;
};

/// Drawe a line to a specific point.
struct LineTo
{
    /// The destination.
    LyonPoint to;
};

/// Draw a cubic bezier curve to the point.
struct CubicBezierTo
{
    /// First control point.
    LyonPoint ctrl1;
    /// Second control point.
    LyonPoint ctrl2;
    /// The destination.
    LyonPoint to;
};

/// Close a path.
struct Close
{};

/// SVG conform path commands
using PathSection = std::variant<MoveTo, LineTo, CubicBezierTo, Close>;

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

/// The SVG path data. It contains a list of instruction (move to, line to, ...).
using PathData = QVector<PathSection>;

std::pair<LyonPoint, LyonPoint> minmax_point(const rust::cxxbridge1::Vec<LyonPoint> points);

LyonPoint operator-(LyonPoint &lrs, const LyonPoint& other);

rust::Box<LyonBuilder> painterBuilder(const QPainterPath &path);
