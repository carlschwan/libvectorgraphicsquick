// SPDX-FileCopyrightText: 2020 Carl Schwan <carlschwan@kde.org>
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "pathgeometry.h"

std::pair<LyonPoint, LyonPoint> minmax_point(const rust::cxxbridge1::Vec<LyonPoint> points)
{
    LyonPoint min { std::numeric_limits<float>::max(), std::numeric_limits<float>::max() };
    LyonPoint max { std::numeric_limits<float>::min(), std::numeric_limits<float>::min() };
    for (const auto &point : points) {
        max = { std::max(max.x, point.x), std::max(max.y, point.y) };
        min = { std::min(min.x, point.x), std::min(min.y, point.y) };
    }
    return std::pair(min, max);
}

LyonPoint operator-(LyonPoint &lrs, const LyonPoint& other)
{
    return LyonPoint {
        lrs.x - other.x,
        lrs.y - other.y,
    };
}

rust::Box<LyonBuilder> painterPathToBuilder(const QPainterPath &path)
{
    auto lyonBuilder = new_builder();
    for (size_t i = 0; i < path.elementCount(); i++) {
        const auto element = path.elementAt(i);
        if (element.isLineTo()) {
            lyonBuilder->line_to(LyonPoint { element.x, element.y });
        } else if (element.isMoveTo()) {
            lyonBuilder->move_to(LyonPoint { element.x, element.y });
        } else if (element.type ==  QPainterPath::ElementType::CurveToElement) {
            // Cubic is encoded with ctrl1 -> CurveToElement, ctrl2 -> CurveToDataElement and to -> CurveToDataElement
            Q_ASSERT(i + 2 < path.elementCount() && "CurveToElement doesn't have data");
            const auto ctrl1 = path.elementAt(i);
            const auto ctrl2 = path.elementAt(i + 1);
            const auto to = path.elementAt(i + 2);
            lyonBuilder->cubic_bezier_to(LyonPoint { ctrl1.x, ctrl1.y }, LyonPoint { ctrl2.x, ctrl2.y }, LyonPoint { to.x, to.y });
            i += 2; // we analysed tree elements instead of just one
        } else {
            Q_ASSERT(false && "Should not happen");
        }
    }

    return lyonBuilder;
}
