/*
 *  Copyright 2020 Carl Schwan <carl@carlschwan.eu>
 */

import QtQuick 2.12

import org.kde.vectoreditor 1.0 as Vector
import org.kde.kirigami 2.4 as Kirigami

Vector.NodeResizeHandle {
    Rectangle {
        color: "grey"
        anchors.fill: parent
        radius: width
        opacity: 0.6
    }
}
