import QtQuick 2.0
import org.kde.kirigami 2.13 as Kirigami
import QtQuick.Controls 2.14 as QQC2
import org.kde.vectoreditor 1.0 as Vector

GenericItem {
    id: root
    property string color: "#ff0000"
    Rectangle {
        antialiasing: true
        id: rectangle
        color: root.color
        anchors.fill: parent
    }
}
 
