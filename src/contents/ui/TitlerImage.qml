import QtQuick 2.0
import QtQuick.Controls 2.14 as QQC2
import org.kde.kirigami 2.13 as Kirigami
import org.kde.vectoreditor 1.0 as Vector

GenericItem {
    id: root
    property alias source: internalImage.source
    Image {
        id: internalImage
        source: "https://kdenlive.org/wp-content/uploads/2016/06/kdenlive-logo-hori.png"
        anchors.fill: parent
    }
}
 
