import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.14 as QQC2
import QtQuick.Layouts 1.15
import org.kde.vectoreditor 1.0 as Vector

Kirigami.ApplicationWindow {
    id: root
    width: 1000
    height: 1000
    visible: true

    title: i18n("LibVectorEditorQuick")

    pageStack.initialPage: Kirigami.Page {
        padding: 0
        Vector.Scene {
            anchors.fill: parent
            id: scene
            pane: Vector.Pane {
                id: pane
                selectedItem: rectangle

                TitlerRectangle {
                    id: rectangle
                    width: 200
                    height: 200
                    x: 20
                    y: 20
                    editor: rectangleEditor
                }
                TitlerImage {
                    width: 200
                    height: 200
                    x: 600
                    y: 600
                    editor: imageEditor
                }
                ShapeItem {
                    x: 450
                    y: 550
                    editor: imageEditor
                }
            }
        }
    }

    contextDrawer: Kirigami.OverlayDrawer {
        modal: !root.wideScreen
        edge: Qt.application.layoutDirection == Qt.RightToLeft ? Qt.LeftEdge : Qt.RightEdge
        handleVisible: true
        onModalChanged: drawerOpen = !modal
        topPadding: 0
        bottomPadding: 0
        leftPadding: 0
        rightPadding: 0

        contentItem: QQC2.ScrollView {
            implicitWidth: Kirigami.Units.gridUnit * 18
            padding: 0
            ColumnLayout {
                Kirigami.AbstractApplicationHeader {
                    Layout.fillWidth: true
                    RowLayout {
                        anchors.fill: parent
                        QQC2.ToolButton {
                            icon.name: "distribute-vertical-y"
                            QQC2.ToolTip.text: i18n("Distribute vertically")
                            QQC2.ToolTip.visible: hovered
                        }
                        QQC2.ToolButton {
                            icon.name: "distribute-horizontal"
                            QQC2.ToolTip.text: i18n("Distribute horizontally")
                            QQC2.ToolTip.visible: hovered
                        }
                        QQC2.ToolButton {
                            icon.name: "align-horizontal-left"
                            QQC2.ToolTip.text: i18n("Align to the left")
                            QQC2.ToolTip.visible: hovered
                        }
                        QQC2.ToolButton {
                            icon.name: "align-horizontal-center"
                            QQC2.ToolTip.text: i18n("Horizontally center")
                            QQC2.ToolTip.visible: hovered
                        }
                        QQC2.ToolButton {
                            icon.name: "align-horizontal-right"
                            QQC2.ToolTip.text: i18n("Align to the right")
                            QQC2.ToolTip.visible: hovered
                        }
                        QQC2.ToolButton {
                            icon.name: "align-vertical-top"
                            QQC2.ToolTip.text: i18n("Align to the top")
                            QQC2.ToolTip.visible: hovered
                        }
                        QQC2.ToolButton {
                            icon.name: "align-vertical-center"
                            QQC2.ToolTip.text: i18n("Verically center")
                            QQC2.ToolTip.visible: hovered
                        }
                        QQC2.ToolButton {
                            icon.name: "align-vertical-bottom"
                            QQC2.ToolTip.text: i18n("Align to the r")
                            QQC2.ToolTip.visible: hovered
                        }
                    }
                }
                GridLayout {
                    Layout.leftMargin: Kirigami.Units.smallSpacing
                    Layout.rightMargin: Kirigami.Units.smallSpacing
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    columns: 6
                    // Position
                    QQC2.SpinBox {
                        Layout.row: 0
                        Layout.column: 0
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        value: pane.selectedItem ? pane.selectedItem.x : 0
                        onValueChanged: pane.selectedItem.x = value
                        to: 1000
                        textFromValue: function(value, locale) {
                            return i18nc("Position", "%1 X", value);
                        }
                    }
                    QQC2.SpinBox {
                        Layout.row: 0
                        Layout.column: 2
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        value: pane.selectedItem ? pane.selectedItem.y : 0
                        onValueChanged: pane.selectedItem.y = value
                        to: 1000
                        textFromValue: function(value, locale) {
                            return i18nc("Position", "%1 Y", value);
                        }
                    }
                    QQC2.SpinBox {
                        Layout.row: 0
                        Layout.column: 4
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        value: pane.selectedItem ? pane.selectedItem.rotation : 0
                        onValueChanged: pane.selectedItem.rotation = value
                        to: 360
                        textFromValue: function(value, locale) {
                            return i18nc("Angle", "%1 °", value);
                        }
                    }
                    QQC2.SpinBox {
                        Layout.row: 1
                        Layout.column: 0
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        value: pane.selectedItem ? pane.selectedItem.width : 0
                        onValueChanged: pane.selectedItem.width = value
                        to: 1000
                        textFromValue: function(value, locale) {
                            return i18nc("W = Width", "%1 W", value);
                        }
                    }
                    QQC2.SpinBox {
                        Layout.row: 1
                        Layout.column: 2
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        value: pane.selectedItem ? pane.selectedItem.height : 0
                        onValueChanged: pane.selectedItem.height = value
                        to: 1000
                        textFromValue: function(value, locale) {
                            return i18nc("H = Height", "%1 H", value);
                        }
                    }
                    QQC2.Button {
                        Layout.row: 1
                        Layout.column: 4
                        icon.name: "object-flip-vertical"
                    }
                    QQC2.Button {
                        Layout.row: 1
                        Layout.column: 5
                        icon.name: "object-flip-horizontal"
                    }
                }
                Loader {
                    sourceComponent: pane.selectedItem.editor
                    onSourceComponentChanged: {
                        item.width = Kirigami.Units.gridUnit * 18
                    }
                }
            }
        }
    }
    Component {
        id: imageEditor 
        ColumnLayout {
            Layout.fillWidth: true
            QQC2.Label {
                Layout.leftMargin: Kirigami.Units.smallSpacing
                Layout.rightMargin: Kirigami.Units.smallSpacing
                font.weight: Font.Bold
                text: i18n("Source")
            }
            RowLayout {
                Layout.fillWidth: true
                Layout.leftMargin: Kirigami.Units.smallSpacing
                Layout.rightMargin: Kirigami.Units.smallSpacing
                QQC2.TextField {
                    Layout.fillWidth: true
                    text: pane.selectedItem.source
                }
                QQC2.Button {
                    icon.name: "document-open"
                }
            }
        }
    }
    Component {
        id: rectangleEditor
        ColumnLayout {
            Layout.fillWidth: true
            QQC2.Label {
                Layout.leftMargin: Kirigami.Units.smallSpacing
                Layout.rightMargin: Kirigami.Units.smallSpacing
                font.weight: Font.Bold
                text: i18n("Color")
            }
            QQC2.TextField {
                Layout.fillWidth: true
                text: pane.selectedItem && pane.selectedItem.color ? pane.selectedItem.color : 0
                onTextEdited: pane.selectedItem.color = text
            }
        }
    }
}
