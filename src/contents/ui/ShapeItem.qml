import QtQuick 2.15
import QtQuick.Shapes 1.15
import QtQuick.Controls 2.14 as QQC2
import org.kde.kirigami 2.13 as Kirigami
import org.kde.vectoreditor 1.0 as Vector

Vector.PathItem {
    id: root
    antialiasing: true
}
