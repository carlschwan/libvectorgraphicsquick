/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Carl Schwan <carl@carlschwan.eu>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 2.14 as QQC2
import org.kde.kirigami 2.13 as Kirigami
import org.kde.vectoreditor 1.0 as Vector


Vector.Item {
    id: root
    
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.TopLeft
        anchors {
            horizontalCenter: parent.left
            verticalCenter: parent.top
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.BottomLeft
        anchors {
            horizontalCenter: parent.left
            verticalCenter: parent.bottom
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.BottomRight
        anchors {
            horizontalCenter: parent.right
            verticalCenter: parent.bottom
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.TopRight
        anchors {
            horizontalCenter: parent.right
            verticalCenter: parent.top
        }
    }
}
