import QtQuick 2.0
import org.kde.vectoreditor 1.0 as Vector
import QtQuick.Controls 2.14 as QQC2

Vector.Item {
    QQC2.TextArea {
        text: "Hello"
        anchors.fill: parent
    }
    
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.TopLeft
        anchors {
            horizontalCenter: parent.left
            verticalCenter: parent.top
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.Left
        anchors {
            horizontalCenter: parent.left
            verticalCenter: parent.verticalCenter
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.BottomLeft
        anchors {
            horizontalCenter: parent.left
            verticalCenter: parent.bottom
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.Bottom
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.bottom
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.BottomRight
        anchors {
            horizontalCenter: parent.right
            verticalCenter: parent.bottom
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.Right
        anchors {
            horizontalCenter: parent.right
            verticalCenter: parent.verticalCenter
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.TopRight
        anchors {
            horizontalCenter: parent.right
            verticalCenter: parent.top
        }
    }
    BasicResizeHandle {
        resizeCorner: Vector.ResizeHandle.Top
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.top
        }
    }
}
 
