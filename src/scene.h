/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include <QQuickItem>

#include "ruler.h"

namespace Vector
{
class Item;
class Pane;

class Scene: public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QQuickItem *pane READ pane WRITE setPane NOTIFY paneChanged)
    
public:
    Scene(QQuickItem *parent = nullptr);
    ~Scene() = default;
    
    void wheelEvent(QWheelEvent *event) override;
    void keyPressEvent(QKeyEvent * event) override;
    void keyReleaseEvent(QKeyEvent * event) override;
    
    Pane *pane() const;
    void setPane(QQuickItem *pane);
    
protected:
    void componentComplete() override;
    
Q_SIGNALS:
    void editorChanged();
    void paneChanged();
    
private:
    bool m_controlPressed = false;
    
    Ruler *m_horizonalRuler = nullptr;
    Ruler *m_verticalRuler = nullptr;
    Pane *m_pane = nullptr;
};

}
