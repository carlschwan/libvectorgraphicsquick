/*
 *   Copyright (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include "item.h"
#include "pathgeometry.h"

namespace Vector
{

class NodeResizeHandle;


class PathItem: public Item
{
    Q_OBJECT

    Q_PROPERTY(QList<QPointF> nodes READ nodes NOTIFY nodesChanged)

public:
    PathItem(QQuickItem *parent = nullptr);
    ~PathItem();

    QList<QPointF> nodes() const;
    void showHandles();
    void hideHandles();

    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;
    void addPathSection(int index, PathSection pathSection);
    void nodePositionChanged();

protected:
    void componentComplete() override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;

private:
    void addHandles(PathSection &pathSection);

Q_SIGNALS:
    void nodesChanged();

private:
    LyonGeometry m_geometry;
    PathData m_commands;
    QList<QPointF> m_nodes;
    QVector<QObject *> m_pathElements;
    QVector<NodeResizeHandle *> m_handles;
    QPointer<QQmlComponent> m_componentHandle;
};
}
